/* global L */

const columnNames = ["OBJECTID", "SiteID", "SiteType", "FullName", "CommonName", "Genus", "Species", "Spread", "Height", "Diameter", "Trunks", "Planted", "PhotoURL", "Address", "Park", "LandUse", "ZoneID", "GrowSpace", "Utility", "Lat", "Lon", "Risk", "Special", "Source", "Appraise", "DateCreated", "DateModified"]

function whitespaceOr(target, replacement){
	return target.match(/\S+/) ? target : replacement
}

async function main(){
  let map = L.map("map").setView([43.538, -96.736], 12)

	/*window.addEventListener("resize", () => {
		map.invalidateSIze()
	})*/

  let imagery = L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
    attribution: "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors | &copy; <a href=\"http://gis-cityofsfgis.opendata.arcgis.com/\">City of Sioux Falls GIS</a>"
  })

  map.addLayer(imagery)

  let markers = L.markerClusterGroup()
	let treeData = await fetch("data/Trees.geojson").then(res => res.json())
	let treeLayer

	function setFilter(fieldName, pattern){
		if(treeLayer != null){
			map.removeLayer(treeLayer)
		}

		treeLayer = L.markerClusterGroup()

		L.geoJSON(treeData, {
			filter: (feature) => {
				if(feature.properties[fieldName] == null){
					return false
				}
				return Boolean(feature.properties[fieldName].match(pattern))
			},
			onEachFeature: (feature, layer) => {
				let props = feature.properties
				let park = props["Park"]
				let fullName = props["FullName"]

				layer.bindPopup(`<dl>
					<dt>Full name</dt>
					<dd>${whitespaceOr(props["FullName"], "<em>Not available<em>")}</dd>
					<dt>Park</dt>
					<dd>${whitespaceOr(props["Park"], "<em>Not available</em>")}</dd>
				</dl>`)
			}
		}).addTo(treeLayer)

		map.addLayer(treeLayer)
	}

	Object.assign(window, {setFilter, treeData})

	let url = new URL(location.href)

	setFilter("FullName", new RegExp(".*" + decodeURIComponent(url.hash.slice(1)) + ".*"))
}



document.addEventListener("DOMContentLoaded", main)
